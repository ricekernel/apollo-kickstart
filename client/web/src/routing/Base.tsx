import * as React from "react";
import { NavBar } from "../components/NavBar";
import { Switch, Route } from "react-router-dom";
import { Login } from "../pages/Login";
import { Register } from "../pages/Register";
import { AuthContainer } from "../components/AuthContainer";
import { PrivateRoute } from "./PrivateRoute";
import { Home } from "../pages/Home";
import styles from "./Base.module.scss";

export const Base = () => {
    return (
        <AuthContainer>
            <div className={styles.base}>
                <NavBar />
                <div className={styles.container}>
                    <Switch>
                        <Route path={"/login"} component={Login} />
                        <Route path={"/register"} component={Register} />
                        <PrivateRoute path={"/"} component={Home} />
                    </Switch>
                </div>
            </div>
        </AuthContainer>
    );
};
