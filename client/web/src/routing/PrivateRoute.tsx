import * as React from "react";
import { Route, RouteProps, Redirect } from "react-router-dom";
import { useAuthContext } from "../components/AuthContainer";

interface Props extends RouteProps {}

export const PrivateRoute = (props: Props) => {
    const { user } = useAuthContext();
    console.log(user);

    if (!user) {
        return <Redirect to={"/login"} />;
    }

    return <Route {...props} />;
};
