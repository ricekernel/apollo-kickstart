import { gql } from "@apollo/client";
import * as ApolloReactCommon from "@apollo/client";
import * as ApolloReactHooks from "@apollo/client";
export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
    ID: string;
    String: string;
    Boolean: boolean;
    Int: number;
    Float: number;
};

export type AdditionalEntityFields = {
    path?: Maybe<Scalars["String"]>;
    type?: Maybe<Scalars["String"]>;
};

export type LoginResponse = {
    __typename?: "LoginResponse";
    token: Scalars["String"];
};

export type Mutation = {
    __typename?: "Mutation";
    register?: Maybe<LoginResponse>;
    login?: Maybe<LoginResponse>;
};

export type MutationRegisterArgs = {
    email: Scalars["String"];
    password: Scalars["String"];
};

export type MutationLoginArgs = {
    email: Scalars["String"];
    password: Scalars["String"];
};

export type Query = {
    __typename?: "Query";
    user?: Maybe<User>;
};

export type QueryUserArgs = {
    id: Scalars["ID"];
};

export type User = {
    __typename?: "User";
    id?: Maybe<Scalars["ID"]>;
    email: Scalars["String"];
    password: Scalars["String"];
};

export type LoginMutationVariables = {
    email: Scalars["String"];
    password: Scalars["String"];
};

export type LoginMutation = { __typename?: "Mutation" } & {
    login?: Maybe<
        { __typename?: "LoginResponse" } & Pick<LoginResponse, "token">
    >;
};

export type RegisterMutationVariables = {
    email: Scalars["String"];
    password: Scalars["String"];
};

export type RegisterMutation = { __typename?: "Mutation" } & {
    register?: Maybe<
        { __typename?: "LoginResponse" } & Pick<LoginResponse, "token">
    >;
};

export const LoginDocument = gql`
    mutation login($email: String!, $password: String!) {
        login(email: $email, password: $password) {
            token
        }
    }
`;
export type LoginMutationFn = ApolloReactCommon.MutationFunction<
    LoginMutation,
    LoginMutationVariables
>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(
    baseOptions?: ApolloReactHooks.MutationHookOptions<
        LoginMutation,
        LoginMutationVariables
    >
) {
    return ApolloReactHooks.useMutation<LoginMutation, LoginMutationVariables>(
        LoginDocument,
        baseOptions
    );
}
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = ApolloReactCommon.MutationResult<
    LoginMutation
>;
export type LoginMutationOptions = ApolloReactCommon.BaseMutationOptions<
    LoginMutation,
    LoginMutationVariables
>;
export const RegisterDocument = gql`
    mutation register($email: String!, $password: String!) {
        register(email: $email, password: $password) {
            token
        }
    }
`;
export type RegisterMutationFn = ApolloReactCommon.MutationFunction<
    RegisterMutation,
    RegisterMutationVariables
>;

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useRegisterMutation(
    baseOptions?: ApolloReactHooks.MutationHookOptions<
        RegisterMutation,
        RegisterMutationVariables
    >
) {
    return ApolloReactHooks.useMutation<
        RegisterMutation,
        RegisterMutationVariables
    >(RegisterDocument, baseOptions);
}
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = ApolloReactCommon.MutationResult<
    RegisterMutation
>;
export type RegisterMutationOptions = ApolloReactCommon.BaseMutationOptions<
    RegisterMutation,
    RegisterMutationVariables
>;
