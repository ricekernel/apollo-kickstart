import * as React from "react";
import { HTMLProps, useState } from "react";
import { motion } from "framer-motion";
import tailwind from "../tailwind";
import { joinClassNames } from "../utils/tailwind";
import styles from "./Input.module.scss";

interface Props extends HTMLProps<HTMLInputElement> {
    error?: string;
}

export const Input = ({
    className,
    placeholder,
    value,
    error,
    ...inputProps
}: Props) => {
    const [isFocused, setIsFocused] = useState(false);

    const variants = {
        focused: { bottom: "70%", fontSize: tailwind.theme.fontSize.xs },
        blurred: { bottom: "0%", fontSize: tailwind.theme.fontSize.base },
    };

    return (
        <div
            className={joinClassNames([
                styles.container,
                error && styles.error,
            ])}
        >
            <div className={styles.wrapper}>
                <motion.label
                    className={styles.label}
                    variants={variants}
                    initial={"blurred"}
                    animate={isFocused || value ? "focused" : "blurred"}
                >
                    {placeholder}
                </motion.label>
                <input
                    {...inputProps}
                    value={value}
                    className={joinClassNames([className, styles.input])}
                    onFocus={() => setIsFocused(true)}
                    onBlur={() => setIsFocused(false)}
                />
            </div>
            {error && <div className={styles.error_message}>{error}</div>}
        </div>
    );
};
