import * as React from "react";
import { createContext, FC, useState } from "react";
import { User } from "../generated/graphql";
import jwt_decode from "jwt-decode";
import { useRequiredContext } from "../hooks/useRequiredContext";

interface AuthContext {
    user?: User;
    decodeToken: (token: string) => void;
}

const AuthContext = createContext<AuthContext | undefined>(undefined);

export const useAuthContext = () => useRequiredContext(AuthContext);

export const AuthContainer: FC = ({ children }) => {
    const [user, setUser] = useState<User>();

    const decodeToken = (token: string) => {
        const { user } = jwt_decode<{ user: User }>(token);
        setUser(user);
    };

    return (
        <AuthContext.Provider value={{ user, decodeToken }}>
            {children}
        </AuthContext.Provider>
    );
};
