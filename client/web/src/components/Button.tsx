import * as React from "react";
import { joinClassNames } from "../utils/tailwind";
import styles from "./Button.module.scss";

type ButtonProps = JSX.IntrinsicElements["button"];

interface Props extends ButtonProps {
    error?: string;
}

export const Button = ({ className, ...buttonProps }: Props) => {
    return (
        <button
            {...buttonProps}
            className={joinClassNames([className, styles.button])}
        >
            Button
        </button>
    );
};
