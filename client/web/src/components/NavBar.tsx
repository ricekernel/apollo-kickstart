import * as React from "react";
import { useState } from "react";
import { toggleState } from "../utils/functions";
import { NavLink } from "react-router-dom";
import styles from "./NavBar.module.scss";
import { joinClassNames } from "../utils/tailwind";

export const NavBar = () => {
    const [menuActive, setMenuActive] = useState(false);

    return (
        <nav className={styles.navbar}>
            <div className={styles.site_name}>
                <NavLink to={"/"} className={styles.site_name_link}>
                    App bootstrap
                </NavLink>
            </div>
            <div className={styles.menu_button_wrapper}>
                <button
                    className={styles.menu_button}
                    onClick={toggleState(setMenuActive)}
                >
                    <svg
                        viewBox={"0 0 20 20"}
                        xmlns={"http://www.w3.org/2000/svg"}
                    >
                        <title>Menu</title>
                        <path
                            d={"M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"}
                        />
                    </svg>
                </button>
            </div>
            <div
                className={joinClassNames([
                    styles.menu_list_container,
                    !menuActive && styles.menu_list_container_hidden,
                ])}
            >
                <ul className={styles.menu_list}>
                    <li>
                        <NavLink
                            to={"/register"}
                            className={styles.menu_link}
                            activeClassName={styles.menu_link_active}
                        >
                            Register
                        </NavLink>
                    </li>
                    <li>
                        <NavLink
                            to={"/login"}
                            className={styles.menu_link}
                            activeClassName={styles.menu_link_active}
                        >
                            Login
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
};
