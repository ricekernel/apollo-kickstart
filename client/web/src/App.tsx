import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { Base } from "./routing/Base";
import {
    ApolloClient,
    InMemoryCache,
    from,
    HttpLink,
    ApolloProvider,
} from "@apollo/client";
import "./assets/scss/normalize.scss";
import { onError } from "@apollo/link-error";

const error = onError(({ networkError }) => {
    if (networkError) console.log(`[Network error]: ${networkError}`);
});

const http = new HttpLink({ uri: "http://localhost:4000/graphql" });

const client = new ApolloClient({
    cache: new InMemoryCache(),
    link: from([error, http]),
});

function App() {
    return (
        <ApolloProvider client={client}>
            <Router>
                <Base />
            </Router>
        </ApolloProvider>
    );
}

export default App;
