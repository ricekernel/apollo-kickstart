import * as React from "react";
import { UserForm } from "./UserForm";
import { useRegisterMutation } from "../generated/graphql";

export const Register = () => {
    const [register, { error }] = useRegisterMutation({ errorPolicy: "all" });

    console.log(error);

    return (
        <UserForm onSubmit={(userData) => register({ variables: userData })} />
    );
};
