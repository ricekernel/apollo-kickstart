import * as React from "react";
import { Input } from "../components/Input";
import { useFormState } from "react-use-form-state";
import { FormEventHandler } from "react";
import { Button } from "../components/Button";
import styles from "./UserForm.module.scss";

interface UserData {
    email: string;
    password: string;
}

interface Props {
    onSubmit: (userData: UserData) => void;
    serverErrors?: { email?: string; password?: string };
}

export const UserForm = ({ onSubmit, serverErrors }: Props) => {
    const [formState, { email, password }] = useFormState<UserData>();

    const handleSubmit: FormEventHandler = (e) => {
        e.preventDefault();
        onSubmit(formState.values);
    };

    return (
        <div className={styles.container}>
            <div className={styles.wrapper}>
                <form onSubmit={handleSubmit}>
                    <Input
                        {...email("email")}
                        required
                        placeholder={"Email"}
                        error={formState.errors.email || serverErrors?.email}
                    />
                    <Input
                        {...password("password")}
                        required
                        placeholder={"Password"}
                        error={
                            formState.errors.password || serverErrors?.password
                        }
                    />
                    <Button type={"submit"}>Submit</Button>
                </form>
            </div>
        </div>
    );
};
