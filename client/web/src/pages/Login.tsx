import * as React from "react";
import { UserForm } from "./UserForm";
import { MutationLoginArgs, useLoginMutation } from "../generated/graphql";
import { useAuthContext } from "../components/AuthContainer";

export const Login = () => {
    const [login, { error }] = useLoginMutation({ onError: () => undefined });
    const { decodeToken } = useAuthContext();

    const errors = error?.graphQLErrors.reduce(
        (serverErrors, graphqlError) => graphqlError.extensions || serverErrors,
        {}
    );

    const handleSubmit = async (userData: MutationLoginArgs) => {
        const { data } = await login({ variables: userData });
        if (!data?.login?.token) return;
        decodeToken(data.login.token);
    };

    return <UserForm onSubmit={handleSubmit} serverErrors={errors} />;
};
