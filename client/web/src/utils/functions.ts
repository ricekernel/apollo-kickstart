import { SetStateAction } from "react";

export const toggleState = (
  setStateFunction: (v: SetStateAction<boolean>) => void
) => () => setStateFunction((currentValue) => !currentValue);
