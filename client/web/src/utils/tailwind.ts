export const joinClassNames = (
    classNames: Array<string | boolean | undefined>
) => classNames.filter(Boolean).join(" ");
