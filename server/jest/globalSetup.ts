import "graphql-import-node";
import "reflect-metadata";
import * as mongoose from "mongoose";

beforeAll(async () => {
  await mongoose.connect(
    (global as any).__MONGO_URI__,
    { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
    (err) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
    }
  );
});

afterAll(async () => {
  await mongoose.disconnect();
});
