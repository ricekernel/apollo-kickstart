module.exports = {
  preset: "@shelf/jest-mongodb",
  transform: {
    "^.+\\.tsx?$": "ts-jest",
    "^.+\\.graphql$": "graphql-import-node/jest",
  },
  globals: {
    "ts-jest": { diagnostics: false },
  },
  setupFilesAfterEnv: ["./jest/globalSetup.ts"],
};
