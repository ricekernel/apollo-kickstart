import "graphql-import-node";
import "reflect-metadata";
import { UserModule } from "./modules/user";

// Get typeDefs from top module, and export it
export default UserModule.schema;
