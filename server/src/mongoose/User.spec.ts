import { User } from "./User";
import { UserDbObject } from "../generated-models";

const createTestUserData = (overrides?: Partial<UserDbObject>) => {
  return {
    password: "Password",
    email: "email@email.com",
    ...overrides,
  };
};

describe("User", function () {
  it("should be able to be saved", async () => {
    const user = new User(createTestUserData());
    await user.save();
    expect(user.isNew).toBe(false);
  });

  describe(".email", () => {
    it("should be required", async () => {
      const user = new User(createTestUserData({ email: undefined }));
      try {
        await user.save();
      } catch (e) {
        expect(e.errors?.email).toBeDefined();
      }
    });

    it("should be unique", async () => {
      const user1 = new User(createTestUserData());
      await user1.save();

      const user2 = new User(createTestUserData());
      try {
        await user2.save();
      } catch (e) {
        console.log(e);
        expect(e.errors?.email).toBeDefined();
      }
    });

    it("should be a valid email", async () => {
      const user = new User(createTestUserData({ email: "notarealemail" }));
      try {
        await user.save();
      } catch (e) {
        expect(e.errors?.email).toBeDefined();
      }
    });
  });

  describe(".password", () => {
    it("should be required", async () => {
      const user = new User(createTestUserData({ password: undefined }));
      try {
        await user.save();
      } catch (e) {
        expect(e.errors?.password).toBeDefined();
      }
    });

    it("should hash the password after saving", async () => {
      const password = "Password";
      const user = new User({ email: "ricky@ricky.com", password });
      await user.save();
      expect(user.password).not.toEqual(password);
    });
  });

  describe(".comparePassword", () => {
    it("should return true when the correct password is provided", async () => {
      const password = "Password";
      const user = await User.create({ email: "ricky@ricky.com", password });

      const match = await user.comparePassword(password);
      expect(match).toBe(true);
    });

    it("should return false when the wrong password is provided", async () => {
      const password = "Password";
      const user = new User({ email: "ricky@ricky.com", password });
      await user.save();

      const match = await user.comparePassword("Different Password");
      expect(match).toBe(false);
    });
  });
});
