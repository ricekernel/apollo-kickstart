import { Document, Model, model, Schema } from "mongoose";
import { UserDbObject } from "../generated-models";
import { genSalt, hash, compare } from "bcrypt";

const SALT_WORK_FACTOR = 10;

export const UserSchema = new Schema(
  {
    email: {
      type: String,
      lowercase: true,
      required: [true, "can't be blank"],
      match: [/\S+@\S+\.\S+/, "is invalid"],
      index: true,
    },
    password: { type: String, required: true },
    image: String,
  },
  { timestamps: true }
);

UserSchema.path("email").validate(function (value) {
  this.model("User").countDocuments({ email: value }, function (err, count) {
    if (err) {
      return err;
    }
    // If `count` is greater than zero, "invalidate"
    return !count;
  });
}, "Email already exists");

UserSchema.pre<User>("save", function (next) {
  let user = this;

  if (!user.isModified("password")) return next();

  genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    hash(user.password, salt, function (err, hash) {
      if (err) return next(err);

      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});

UserSchema.methods.comparePassword = function (candidatePassword) {
  return compare(candidatePassword, this.password);
};

UserSchema.virtual("id").get(function () {
  return this._id.toHexString();
});

export interface User extends Omit<UserDbObject, "_id">, Document {
  comparePassword: (candidate: string) => Promise<boolean>;
}
interface UserModel extends Model<User> {}

export const User = model<User, UserModel>("User", UserSchema);

const t: UserModel = "" as any;

const f = async () => {
  const u = await User.create({});
};
