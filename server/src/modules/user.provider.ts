import { Injectable } from "@graphql-modules/di";
import { User } from "../mongoose/User";
import { UserDbObject } from "../generated-models";

@Injectable()
export class UserProvider {
  createUser(user: Partial<UserDbObject>) {
    return User.create(user);
  }
  getUserById(id: string) {
    return User.findById(id).exec();
  }
  getUserByEmail(email: string) {
    return User.findOne({ email }).exec();
  }
}
