import { UserProvider } from "./user.provider";
import { Resolvers } from "../generated-models";
import { UserInputError } from "apollo-server";
import { sign } from "jsonwebtoken";

export const UserResolvers: Resolvers = {
  Mutation: {
    register: async (_, data, { injector }) => {
      const user = await injector
        .get<UserProvider>(UserProvider)
        .createUser(data);
      if (user) {
        const token = await sign({ user }, process.env.JWT_SECRET);
        return { token };
      }
    },
    login: async (_, data, { injector }) => {
      const user = await injector
        .get<UserProvider>(UserProvider)
        .getUserByEmail(data.email);

      if (!user) {
        throw new UserInputError("Invalid login", {
          email: "Email or Password is incorrect",
        });
      }

      const passwordsMatch = await user.comparePassword(data.password);

      if (!passwordsMatch) {
        throw new UserInputError("Invalid login", {
          email: "Email or Password is incorrect",
        });
      }

      const token = await sign({ user }, process.env.JWT_SECRET);
      return { token };
    },
  },
  Query: {
    user: (root, { id }, { injector }) => {
      return injector.get<UserProvider>(UserProvider).getUserById(id);
    },
  },
};
