import { GraphQLModule } from "@graphql-modules/core";
import * as typeDefs from "./user.schema.graphql";
import { UserResolvers } from "./user.resolvers";
import { UserProvider } from "./user.provider";

export const UserModule = new GraphQLModule({
  typeDefs,
  resolvers: UserResolvers,
  providers: [UserProvider],
});
