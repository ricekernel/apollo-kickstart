import { execute } from "graphql";
import { gql } from "apollo-server";
import { UserModule } from "./user";

describe("UserModule", () => {
  it("FieldResolver of Query: userById", async () => {
    const { schema } = UserModule;

    const result = await execute({
      schema,
      document: gql`
        query {
          user(id: "ANOTHERID") {
            id
            email
          }
        }
      `,
    });
    expect(result.errors).toBeFalsy();
    expect(result.data["userById"]["id"]).toBe("ANOTHERID");
    expect(result.data["userById"]["email"]).toBe("email@email.com");
  });
});
