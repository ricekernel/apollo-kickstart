import { GraphQLModule } from "@graphql-modules/core";
import { UserModule } from "./user";

export const App = new GraphQLModule({
  imports: [UserModule],
});
