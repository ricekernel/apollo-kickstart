import "graphql-import-node";
import "reflect-metadata";
import { ApolloServer } from "apollo-server";
import { App } from "./modules/app";
import * as mongoose from "mongoose";
import { verify } from "jsonwebtoken";
import { User } from "./generated-models";

if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

mongoose.connect(
  `mongodb://barfly_web:${process.env.MONGO_PASS}@ds034677.mlab.com:34677/barfly`,
  {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  }
);

const isUserToken = (
  parsedToken: string | {}
): parsedToken is { user: User } => {
  return true;
};

const server = new ApolloServer({
  schema: App.schema,
  context: ({ req, connection }) => {
    const { authorization: token = "" } = req?.headers || connection.context;

    try {
      if (token) {
        const parsedToken = verify(token, process.env.JWT_SECRET);
        if (isUserToken(parsedToken)) {
          return {
            token,
            user: parsedToken.user,
          };
        }
      }
    } catch (e) {
      console.warn(`Unable to authenticate using auth token: ${token}`);
    }

    return {
      token,
    };
  },
});

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
